export class Subnet {
    id: number;
    address: string;
    cidr: string;
    ips: object;
}