import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-subnet-display',
  templateUrl: './subnet-display.component.html',
  styleUrls: ['./subnet-display.component.css']
})
export class SubnetDisplayComponent implements OnChanges, OnInit {
  @Input() subnetitem;
  @Input() globalSelected;
  @Output() selectedSubnet: EventEmitter<any> = new EventEmitter<any>();
  subnetIps: [];

  subnet;

  selectedIp: null;

  selectedItem: '';

  currentClass = 'ip-selector';

  public selectIp(ip: any):void {
    this.selectedIp = ip;
    this.selectedSubnet.emit(this.selectedIp);
  }

  public getItemClass(id) {

    return this.selectedItem == id ? 'ip-selector ip-selected' : 'ip-selector';
  }

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    let tempSelected = changes.globalSelected;
    if (tempSelected.currentValue) {
      this.selectedItem = tempSelected.currentValue;
      if (this.selectedItem === this.subnetitem.address) {
        this.currentClass = 'ip-selector ip-selected';
      }
    }
  }

  ngOnInit() {
    if (this.subnetitem) {
      this.subnet = this.subnetitem;
      this.subnetIps = this.subnetitem.ips;
    }
  }

}
