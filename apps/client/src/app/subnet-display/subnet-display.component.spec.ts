import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubnetDisplayComponent } from './subnet-display.component';

describe('SubnetDisplayComponent', () => {
  let component: SubnetDisplayComponent;
  let fixture: ComponentFixture<SubnetDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubnetDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubnetDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
