import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getSubnets() {
    return this.http.get('http://localhost:82/subnets')
  }

  getIp(id) {
    let getPath = 'http://localhost:82/ip/' + id;

    return this.http.get(getPath);
  }
}
