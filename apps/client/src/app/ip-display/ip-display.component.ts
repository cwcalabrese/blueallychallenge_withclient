import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DataService} from "../data.service";

@Component({
  selector: 'app-ip-display',
  templateUrl: './ip-display.component.html',
  styleUrls: ['./ip-display.component.css']
})
export class IpDisplayComponent implements OnChanges, OnInit {
  @Input() ipId;

  ip = null;

  constructor(private data: DataService) { }

  ngOnChanges(changes: SimpleChanges): void {
    let tempIp = changes.ipId;
    if (tempIp.currentValue) {
      this.data.getIp(tempIp.currentValue).subscribe(data => {
        this.ip = data;
      })
    }
  }

  ngOnInit() {

  }

}
