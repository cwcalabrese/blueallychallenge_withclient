import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubnetIpListComponent } from './subnet-ip-list.component';

describe('SubnetIpListComponent', () => {
  let component: SubnetIpListComponent;
  let fixture: ComponentFixture<SubnetIpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubnetIpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubnetIpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
