import {Component, EventEmitter, OnInit, Output, SimpleChange} from '@angular/core';
import {DataService} from "../data.service";

@Component({
  selector: 'app-subnet-list',
  templateUrl: './subnet-list.component.html',
  styleUrls: ['./subnet-list.component.css']
})
export class SubnetListComponent implements OnInit {
  @Output() selectedIp: EventEmitter<any> = new EventEmitter<any>();
  selected = null;
  subnets = null;

  public updateSelected(ip: any):void {
    this.selected = ip;
    this.selectedIp.emit(this.selected);
  }

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getSubnets().subscribe(data => {
      this.subnets = data;
    })
  }

}
