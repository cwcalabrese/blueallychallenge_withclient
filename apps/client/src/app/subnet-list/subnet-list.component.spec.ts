import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubnetListComponent } from './subnet-list.component';

describe('SubnetListComponent', () => {
  let component: SubnetListComponent;
  let fixture: ComponentFixture<SubnetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubnetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubnetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
