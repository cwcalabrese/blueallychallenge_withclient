import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatProgressSpinnerModule } from "@angular/material";

import { AppComponent } from './app.component';
import { SubnetListComponent } from './subnet-list/subnet-list.component';
import { IpDisplayComponent } from './ip-display/ip-display.component';
import { SubnetDisplayComponent } from './subnet-display/subnet-display.component';
import { SubnetIpListComponent } from './subnet-ip-list/subnet-ip-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SubnetListComponent,
    IpDisplayComponent,
    SubnetDisplayComponent,
    SubnetIpListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
