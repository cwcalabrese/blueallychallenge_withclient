<?php
namespace App\Controller;

use App\Repository\IpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class IpController extends ApiController
{
    /**
     * @Route("/ips", methods="GET")
     */
    public function ipsAction(IpRepository $ipRepository)
    {
        $ips = $ipRepository->transformAll();

        return $this->respond($ips);
    }

    /**
     * @Route("/ip/{id}", methods="GET")
     */
    public function ipAction($id, IpRepository $ipRepository)
    {
        $ip = $ipRepository->find($id);

        $transformed = $ipRepository->transform($ip);

        return $this->respond($transformed);
    }
}