<?php
namespace App\Controller;

use App\Repository\SubnetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class SubnetController extends ApiController
{
    /**
     * @Route("/subnets", methods="GET")
     */
    public function subnetsAction(SubnetRepository $subnetRepository)
    {
        $subnets = $subnetRepository->transformAll();

        return $this->respond($subnets);
    }

    /**
     * @Route("/subnet/{id}", methods="GET")
     */
    public function subnetAction($id, SubnetRepository $subnetRepository)
    {
        $subnet = $subnetRepository->find($id);

        $transformed = $subnetRepository->transform($subnet);

        return $this->respond($transformed);
    }
}