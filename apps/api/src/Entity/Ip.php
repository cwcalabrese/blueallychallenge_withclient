<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IpRepository")
 */
class Ip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_tag;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subnet", inversedBy="ips")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subnet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressTag(): ?string
    {
        return $this->address_tag;
    }

    public function setAddressTag(string $address_tag): self
    {
        $this->address_tag = $address_tag;

        return $this;
    }

    public function getSubnet(): ?Subnet
    {
        return $this->subnet;
    }

    public function setSubnet(?Subnet $subnet): self
    {
        $this->subnet = $subnet;

        return $this;
    }
}
